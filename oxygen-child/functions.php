<?php
/**
 *	Oxygen WordPress Theme
 *	
 *	Laborator.co
 *	www.laborator.co 
 */

// This will enqueue style.css of child theme
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'oxygen-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/custom.js' );
}

function my_hide_shipping_when_free_is_available( $rates ) {
 $free = array();
 foreach ( $rates as $rate_id => $rate ) {
 if ( 'free_shipping' === $rate->method_id ) {
 $free[ $rate_id ] = $rate;
 break;
 }
 }
 return ! empty( $free ) ? $free : $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );

add_filter( 'woocommerce_variable_sale_price_html', 'hide_variable_max_price', PHP_INT_MAX, 2 );
add_filter( 'woocommerce_variable_price_html',      'hide_variable_max_price', PHP_INT_MAX, 2 );
function hide_variable_max_price( $price, $_product ) {
    $min_price_regular = $_product->get_variation_regular_price( 'min', true );
    $min_price_sale    = $_product->get_variation_sale_price( 'min', true );
    return ( $min_price_sale == $min_price_regular ) ?
        wc_price( $min_price_regular ) :
        '<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';
}

add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'mmx_remove_select_text'); function mmx_remove_select_text( $args ){ $args['show_option_none'] = ''; return $args; }

add_filter( 'woocommerce_billing_fields', 'kd_no_required_phone', 10, 1 );

function kd_no_required_phone( $address_fields ) {
	$address_fields['billing_phone']['required'] = false;
	return $address_fields;
}

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'id' => 'shop_filter_sidebar',
    'name' => 'Filter Sidebar',
    'before_widget' => '<div id="%1$s" class="sidebar widget %2$s %1$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>'  
  )
);

/**
 * Auto update cart after quantity change
 *
 * @return  string
 **/
add_action( 'wp_footer', 'cart_update_qty_script' );
function cart_update_qty_script() {
  if (is_cart()) :
   ?>
    <script>
        jQuery('div.woocommerce').on('change', '.qty', function(){
           jQuery("[name='update_cart']").removeAttr('disabled');
           jQuery("[name='update_cart']").trigger("click"); 
        });
   </script>
<?php
endif;
}

function wpchris_override_woo_checkout_scripts() {
  wp_deregister_script('wc-checkout');
  wp_enqueue_script('wc-checkout', get_stylesheet_directory_uri() . '/woocommerce/js/checkout.js', array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true);
}
add_action('wp_enqueue_scripts', 'wpchris_override_woo_checkout_scripts');

function wpchris_unselect_payment_method() {
  echo "<script>jQuery( '.payment_methods input.input-radio' ).removeProp('checked');</script>";
}
add_action('woocommerce_review_order_before_submit','wpchris_unselect_payment_method' );

function wpchris_filter_gateways( $gateways ){
  global $woocommerce;

  foreach ($gateways as $gateway) {
      $gateway->chosen = 0;
  }

  return $gateways;
}
add_filter( 'woocommerce_available_payment_gateways', 'wpchris_filter_gateways', 1);